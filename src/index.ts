import { combine, isUrlAbsolute, TimelinePipe } from '@pnp/core'
import { DefaultHeaders, DefaultInit, spfi } from '@pnp/sp'
import { NodeFetchWithRetry, SPDefault } from '@pnp/nodejs'
import { DefaultParse, InjectHeaders, Queryable } from '@pnp/queryable'
import { createReadStream } from 'fs'
import { readdir, readFile, stat } from 'fs/promises'
import { IFolder } from '@pnp/sp/folders'
import { basename, join } from 'path'

import '@pnp/sp/webs/index.js'
import '@pnp/sp/folders/index.js'
import '@pnp/sp/files/index.js'
import '@pnp/sp/sites/index.js'
import '@pnp/sp/items/index.js'
import '@pnp/sp/lists/index.js'

import { RequestDigest } from './request-digest.js'
import { exit } from 'process'

// Read config
interface Config {
    baseUrl: string
    cookie: string
    folderRelativeUrl: string
}

const config: Config = JSON.parse(await readFile('./config.json', 'utf8'))

// Read arguments
const args = process.argv.slice(2)
const fileToUpload = args[0]

export function BaseUrl(baseUrl: string): TimelinePipe<Queryable> {
    return (instance) => {
        instance.on.pre.prepend(async (url, init, result) => {
            if (!isUrlAbsolute(url)) {
                url = combine(baseUrl, url)
            }
            console.log(url.replace(config.baseUrl, ""))
            return [url, init, result]
        })
        return instance
    }
}

const sp = spfi()
    .using(
        DefaultHeaders(),
        DefaultInit(),
        NodeFetchWithRetry(),
        DefaultParse(),
        BaseUrl(config.baseUrl),
        InjectHeaders({ 'cookie': config.cookie }),
        RequestDigest(),
    )

let rootFolder = sp.web.getFolderByServerRelativePath(config.folderRelativeUrl)
rootFolder = rootFolder.folders.getByUrl('upload-script-test')
rootFolder = (await rootFolder.folders.addUsingPath('test', true)).folder

async function Upload(path: string, folder: IFolder) {
    const fileStat = await stat(path)
    const fileName = basename(path)

    if (fileStat.isDirectory()) {
        const childFolder = await folder.folders.addUsingPath(fileName, true)

        for (const childFile of await readdir(path)) {
            await Upload(join(path, childFile), childFolder.folder)
        }
    }

    if (fileStat.isFile()) {
        const fileStream = createReadStream(path)
        await folder.files.addChunked(fileName, fileStream, undefined, true)
        fileStream.close()
    }

}

Upload(fileToUpload, rootFolder)
